const mongodb = require('mongodb'),
  request = require('request'),
  cheerio = require('cheerio');

const MongoClient = mongodb.MongoClient;

// scraping `bg` now
const BOOK = 'bg';

// arguments to start processing
const CHAPTER = process.argv[2], VERSE = process.argv[3];
if (!CHAPTER || !VERSE) {
  throw new Error("Please pass chapter and verse in arguments");
}

// mongo_url
const MONGOURL = 'mongodb://localhost:27017/vedabase';

MongoClient.connect(MONGOURL, (err, db) => {
  if (err) {
    throw new Error(`Unable to connect to the mongoDB server. Error: ${err.message}`);
  }
  // start scrapping
  scrape(BOOK, `http://www.vedabase.com/en/bg/${CHAPTER}/${VERSE}`, db);
});

function validation(raw_url) {
  // `2/12-14`, `2/14`
  const matched = raw_url.match(/[0-9]{1,2}\/[0-9]{1,2}([-]{1}[0-9]{1,2})?$/);
  if (!matched) {
    return {
      done: false
    };
  }
  return {
    done: true,
    chapter: chapter,
    verse: verse,
    url: raw_url
  };
}

function scrape(book, raw_url, db) {
  const validated = validation(raw_url);
  if (!validated.done) {
    throw new Error(`Url validation failed : ${raw_url}`);
  }
  parsePage(validated.url, validated.verse, db);
}

function getVerse(raw_url) {
  const validated = validation(raw_url);
  if (!validated.done) {
    return null;
  }
  return {
    chapter: validation.chapter,
    verse: validation.verse
  };
}

function parsePage(url, verse, db) {
  request(url, function (err, response, html) {
    if (err) {
      throw new Error(err);
    }

    const $ = cheerio.load(html);

    let doc = {
      chapter: CHAPTER,
      verse: verse,
      text: [],
      synonyms: []
    };

    // title
    const title = $("#page-title").text();
    if (title.match(/Page not found/)) {
      throw new Error(`Encountered 404 page : ${url}`);
    }

    // page specific logic to get selector for content
    const node = $("body").attr('class').match(/node-[0-9]+\b/);
    if (!node) {
      throw new Error(`No node found : ${url}`);
    }
    const page = node[0];
    // content element
    const content = $(`#${page} > .content`);

    // start parsing
    console.log(`Parsing ${url} : ${title}`);

    // parse verse
    content.find('.field-name-field-verse-text > .field-items').find('.field-item').contents().each( function(i, item) {
      $(item).contents().each( (j, value) => {
        if (value.children && value.children.length) {
          value.children.forEach( (el, index) => {
            el.data && doc.text.push(el.data);
          });
        } else {
          value.data && doc.text.push(value.data);
        }
      });
    });

    // parse synonyms
    let word = "";
    content.find('.field-name-field-synonyms > .field-items').find('.field-item').contents().each( function() {
      if (this.nodeType === 1) {
        word = $(this).text();
      } else if (this.nodeType === 3) {
        const meaning = $(this).text().replace(/—|;/g, '').trim();
        doc.synonyms.push({
          word,
          meaning
        });
        word = "";
      }
    });

    const previous_url = 'http://www.vedabase.com' + $(`#${page} > .verse-pager`).find('.previous > a').prop('href');
    const next_url = 'http://www.vedabase.com' + $(`#${page} > .verse-pager`).find('.next > a').prop('href');

    doc.title = title;
    doc.translation = content.find('.field-name-field-translation > .field-items').text();
    doc.summary = content.find('.field-type-text-with-summary > .field-items').text();
    doc.url = url;
    doc.created_at = Date.now();
    doc.previous = getVerse(previous_url);
    doc.next = getVerse(next_url);

    db.collection(BOOK).insert(doc, {w:1}, function (err, data) {
      setTimeout(function() {
        scrape(BOOK, next_url, db);
      }, 1000);
    });
  });
}
