const mongodb = require('mongodb');

const MongoClient = mongodb.MongoClient;

// mongo_url
const MONGOURL = 'mongodb://localhost:27017/vedabase';

MongoClient.connect(MONGOURL, (err, db) => {
  if (err) {
    throw new Error(`Unable to connect to the mongoDB server. Error: ${err.message}`);
  }
  reset(db);
});

function reset(db) {
  let cursor = db.collection('bg0').find();

  let prev = null;

  cursor.each( (err, item) => {
    if (item == null) {
      cursor.toArray( (err, items) => {
        db.close();
      });
    } else {
      next_doc = db.collection('bg0').findOne({ _id: { $gt: item._id } }, (err, result) => {
        let next = result ? `${result.chapter}.${result.verse}` : null;
        let doc = Object.assign(item, { prev, next });
        console.log(doc.prev, `${doc.chapter}.${doc.verse}`, doc.next);
        db.collection('bg').insert(doc, { w: 1 });
        prev = `${item.chapter}.${item.verse}`;
      });
    }
  });

}
